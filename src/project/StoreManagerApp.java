//Yosef Raya CS_201 4/28/2022 Final Project 
//StoreManager Application
//File that has main method and menu methods
package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreManagerApp {

//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//Read file method to extract data from stock file and puts it in our InStock class to access data
	//used in main method 
	public static InStock readData(String filename){
		InStock inventory = new InStock();
		
		try {
				File data = new File(filename);
				Scanner input = new Scanner (data);
				
				while (input.hasNextLine()) {
				try {
				String line = input.nextLine();
				String [] value = line.split(",");
				//I am trying to connect this part with the toString method in my InStock class
				
					Item s = new Item();
					if (value.length < 3) {
					s = new Item(value[0], Double.parseDouble(value[1]));}
					
					else if (value[2].contains("/")) {s = new Item(value[0], Double.parseDouble(value[1]), value[2]);}
					
					else if (value[2] != null) {s = new Item(value[0], Double.parseDouble(value[1]), Integer.parseInt(value[2]));}
					
					inventory.addItem(s);	
					
					} catch (Exception e) {}
				} 
				input.close();
			} catch (FileNotFoundException fnf) {System.out.println("File could not be found");}
		
					catch(Exception e) {System.out.println("Error occurred reading in file.");}
		
		return inventory;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
	//this method is for option 4.Go shopping, 
	//it allows shoppers to add items to their cart, apply discounts, validates user age when necessary, calculates total, and removes Items 
	//from inventory when the shopper checks out. 
	public static void goShopping(InStock inventory) { 
		InStock items = new InStock();
		Scanner input = new Scanner(System.in);
		boolean done = false;
		int shopperAge = 0;
		Age r = new Age(shopperAge);
		r.validUserAge();
		do {
		System.out.print("Please enter the name of the item you'd like to add to your cart: ");
		String name = input.nextLine();
		int position = -1;
		boolean found = false;
		Item t= new Item();
		
		for (int i=0; i<inventory.getSize();i++) {
			String next = inventory.getItemFromInStock(i).getName();
			if (next.equals(name)) {
				position = i;
				found = true;
				break;}	
			}
		if (found) {t = inventory.getItemFromInStock(position);
		} else {System.out.println("Item was not found, please view inventory (option 1) from main menu to check for available Items" + "\n");
		break;}
		
		String result = t.toString2();
		System.out.println("\n" + "Item information: " + result);
		
		if(t.getAge() == 18 || t.getAge() == 21) {
			t.validUserAge();
			int userAge = Integer.parseInt(input.nextLine());
			if (userAge < t.getAge()) {System.out.println("You cannot add this item to your cart, you are under the age required");
			break;}
			}	else {done = false;}
				
		
		items.addItem(t);
		System.out.println("Items in your cart: ");
		for (int i=0;i<items.getSize();i++) {
			System.out.println(items.getItemFromInStock(i));
		}
		
			System.out.println("Would you like to add another Item to your cart?(Yes/No)");
			String answer = input.nextLine();
			
			if (answer.equals("no") || answer.equals("No") || answer.equals("n") || answer.equals("N") || answer.equals("NO")) {
				double total = total(items);
				System.out.println("Your total is: " + "$" + total);
				
				System.out.println("\n" + "Highschool students get a 20% discount, do you think you qualify?(Yes/No)");
				String qualify = input.nextLine();
				if (qualify.equals("yes") || qualify.equals("Yes") || qualify.equals("y") || qualify.equals("Y") || qualify.equals("YES")) {
				int customerAge = Integer.parseInt(input.nextLine());
				Age a = new Age(customerAge);
				double newTotal = a.discount(total);
				System.out.println("Your new total is " + newTotal);}
				else if ((qualify.equals("no") || qualify.equals("No") || qualify.equals("n") || qualify.equals("N") || qualify.equals("NO")) ) {
					System.out.println("Do you have any copouns you'd like to apply to your order?(Yes/No)");
						String copoun = input.nextLine();
						if (copoun.equals("no") || copoun.equals("No") || copoun.equals("n") || copoun.equals("N") || copoun.equals("NO")) {
										System.out.println("Your total is: " + "$" + total);}
						else if (copoun.equals("yes") || copoun.equals("Yes") || copoun.equals("y") || copoun.equals("Y") || copoun.equals("YES")) {
							System.out.println("Please Enter the discount you have, enter number only");
							System.out.println("For example: if your discount is 20%, enter 20");
							double dis = (Double.parseDouble(input.nextLine()))/100;
							double discountedTotal = calcDiscount(total, dis);
							System.out.println("your new total is: " + discountedTotal);
						} else {System.out.println("Invalid entry, please only choose Yes or no, otherwise program will return to main menu");
						break;}
				}
				
				for (int i=0;i<items.getSize();i++) {
					inventory.removeItem(items.getItemFromInStock(i));
				}
				done = true;
			} else if (answer.equals("yes") || answer.equals("Yes") || answer.equals("y") || answer.equals("Y") || answer.equals("YES")) {
				done = false;
			}	else { System.out.println("Your entry is invalid, please only use Yes or no, else program will return to main menu");
			done = true;}
			
		}while(!done);
		
		
		}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------		
	//method used to calculate total of shopped items before checking out
	//used in option 4. Go shopping
	public static double total(InStock inCart) {
		double sum =0;
		for (int i=0; i<inCart.getSize();i++) {
			sum+= inCart.getItemFromInStock(i).getPrice();
		}
		
		return sum;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//method used to calculate discounted total of shopped items before checking out
	//used in option 4. Go shopping
	public static double calcDiscount(double total, double discount) {					
		double updated_price = total - (total * discount);
		return updated_price;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//method used to search for an item and its quantity, option 3. Search for an item
	public static void searchItem(InStock inventory) {
		System.out.println("Please enter the name of the Item you want to search for: ");
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		boolean found = false;
		int position = -1;
		
		for (int i=0; i<inventory.getSize();i++) {
			String next = inventory.getItemFromInStock(i).getName();
			if (next.equals(name)) {
				found = true;
				position = i;
				break;}
		}
		if (found) {System.out.println("item found: " + inventory.getItemFromInStock(position).toString2() + 
				", Quantity: " + inventory.getNumberOfItems(inventory.getItemFromInStock(position).getName()));
		} else {System.out.println("Item was not found, please try again");}
		
		
		}
		
	
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//menu method that hosts all options
	public static InStock menu(InStock inventory){
		Scanner input = new Scanner(System.in);
		int choice = 0;  
		
		do {
			try {
			System.out.println("Please choose one of the following options: ");
			String [] options = {"View inventory", "Add an Item to inventory", "Search for an item", "Go Shopping", "Modify an existing item", "Exit and save to file"};
			for (int i=0; i<options.length; i++) {									 
				System.out.println((i+1) + ". " + options[i]);}
			
			choice = Integer.parseInt(input.nextLine());} catch (NumberFormatException e) {System.out.println("Please enter an Integer!");}
			
			if (choice == 1) {viewInventory(inventory);}							
			else if (choice == 2) {addItemToInventory(inventory);}
			else if (choice == 3) {searchItem(inventory);}
			else if (choice == 4) {goShopping(inventory);}
			else if (choice == 5) {modifyExistingItem(inventory);}		
			else if (choice != 1 && choice !=2 && choice !=3 && choice != 4 && choice != 5)
			{System.out.println("Inalid entry, please choose one of the options displayed using a number");
			}
		}  while (choice != 6);
		
		input.close();
		return inventory;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//This method is option 2. Add Item to inventory
	//User can add new items and include all necessary details   
	public static InStock addItemToInventory(InStock inventory) {
		Scanner input = new Scanner(System.in);
		Item t;
		
		System.out.print("Please enter the Item's Name you'd like to add: ");
		String name = input.nextLine();
		
		System.out.println("Please enter Item Price: ");
		double price = 0;
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {System.out.println("Invalid entry, return to main menu");
		
		return inventory;}
		
		System.out.println("Does the Item have an expiry date?(Yes or No)");
		String answer = input.nextLine();
		
		if (answer.equals("yes") || answer.equals("Yes")) {System.out.println("Please enter expiry date using format (month/day/year)");
															String date = input.nextLine();
														     t = new Item(name, price, date);
														     inventory.addItem(t);}
		
		else if (answer.equals("no") || answer.equals("No")) {System.out.println("are there any age restrictions on the Item? (Yes or No)");
																answer = input.nextLine();
																if (answer.equals("yes") || answer.equals("Yes")) {System.out.println("Please Enter age: ");
																int age = Integer.parseInt(input.nextLine());
																t = new Item(name, price, age);
																inventory.addItem(t);}
																else if (answer.equals("no") || answer.equals("No")) {t = new Item(name, price);
																inventory.addItem(t);}}
		System.out.println("Item has been added to inventory");
		
		return inventory;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//This method is for option 5. Modify an existing item in the inventory
	//It pulls the item from the file, modifies it, and then apply the modification to all instances
	public static InStock modifyExistingItem(InStock inventory) {
		System.out.println("Please enter the name of the Item you want to modify: ");
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		boolean found = false;
		int position = -1;
		
		for (int i=0; i<inventory.getSize();i++) {
			String next = inventory.getItemFromInStock(i).getName();
			if (next.equals(name)) {
				found = true;
				position = i;
				break;}
		}
		if (found) {System.out.println("item found: " + inventory.getItemFromInStock(position).toString2());
		} else {System.out.println("Item was not found, please try again");}
		Item t = inventory.getItemFromInStock(position);
		
		System.out.println("What would you like to modify: " + "(Name or Price)");
		String answer = input.nextLine();
		if (answer.equals("name") || answer.equals("Name") || answer.equals("NAME")) {
			System.out.println("Please enter new name: ");
			String newName = input.nextLine();
			
			if (name.equals(t.getName())) {
			inventory.chagneNameOfItems(name, newName);
			System.out.println("Name of Item has been changed successfully" + "\n");}
		}
		else if (answer.equals("price") || answer.equals("Price") || answer.equals("PRICE")) {
			double price = t.getPrice();
			System.out.println("Please enter new price: ");
			double newPrice = Double.parseDouble(input.nextLine());
			if (name.equals(t.getName())) {
				inventory.chagnePriceOfItems(price, newPrice);
				System.out.println("Price of Item has been changed successfully" + "\n");}
		}
		return inventory;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------		
	//This is the save to file method, it is activated every time the user exits the program option 6. Exit the program and save to file
	public static void saveFile(String filename, InStock inventory) {
	try {
			FileWriter writer = new FileWriter("src/project/stock.csv");
			for (int i=0; i<inventory.getSize(); i++) {
				writer.write(inventory.getItemFromInStock(i).toString() + "\n");
				
			}
			writer.close();
	} catch (Exception e) {System.out.print("Error while tring to save to file");}
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------	
	//this method is used to view items in the store inventory, used for option 1. view inventory
public static InStock viewInventory(InStock inventory) {
	for (int i = 0; i<inventory.getSize(); i++) {
			System.out.println(inventory.getItemFromInStock(i).toString());
			}
	return inventory;
}	
	
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
	public static void main(String[] args) {
		//call method to read in system details
		InStock inventory = readData("src/project/stock.csv");				//pass the file to be stored in InStock
		inventory = menu(inventory);
		saveFile("src/project/stock.csv", inventory);
		System.out.println("Goodbye!");
		
	}

	
}
