//Yosef Raya CS_201 4/28/2022 Final Project 
//InStock class is the class that encapsulates the Item class in an arrayList for custom manipulation and easier access to stock file
package project;

import java.util.ArrayList;
import java.util.Scanner;



public class InStock {							//needs more work and testing
	private  ArrayList<Item> items;				//I think this attribute should be a array to contain the data

	
	//default constructor
	public InStock() {
	
		items = new ArrayList<Item>();
		
	}
	//non-default constructor 
	public InStock(ArrayList<Item> item) {	
		this.items = new ArrayList<Item>(item.size());
		for (Item o : item) {
			this.items.add(o);}
	}
	
	//to get an item from the inventory using its index
	//used in almost in all options
	public Item getItemFromInStock(int index) {
		return items.get(index);
	}
	
	//get the size of the arrayList of items
	//important to use in for loops
	public int getSize() {
		return items.size();
	}
	
	//this method is used to add items to the store inventory to be saved to InStock
	//used in option 2. Add an item to inventory 
	public void addItem(Item item) {
		items.add(item);
	}
	
	//this method is used to remove items from the inventory to be removed from the stock file
	//used in option 4. go shopping to remove items from inventory after checking out
	public void removeItem(Item item) {
		items.remove(item);
	}
	
	//to check available quantity of a certain item in stock
	//used in option 3. seaarch for an item
	public int getNumberOfItems(String name) {
		int quantity = 0;
		for (Item t: items) {
			if (t.getName().equals(name)) {
				quantity++;
			}
		}
		return quantity;
	}
	
	//to change name for duplicate items 
	//used in option 5. modify an existing item
	public void chagneNameOfItems(String name, String newName) {
			
		for (Item t: items) {
			if (name.equals(t.getName())) {
			t.setName(newName);}
			}
		
	}
	
	//to change price for duplicate items
	//used in option 5. modify an existing item
	public void chagnePriceOfItems(double price, double newPrice) {
		
		for (Item t: items) {
			if (price == (t.getPrice())) {
			t.setPrice(newPrice);}
			}
		
	}
		
	
	
	//to view items In Stock Items
	public String csvData() {
		String result = null;
		
		
		for(Item t: items) {
			
			 if (t.getDate() == null) {
				 if (t.getAge() < 18) {result = t.getName() + "," + t.getPrice();}
				 else if ((t.getAge() == 18 || t.getAge() == 21) && t.getDate() == null) {result =  t.getName() + "," + t.getPrice() + "," + t.getAge();}
				 }
			 
			else if (t.getDate().contains("/")) {result = t.getName() + "," + t.getPrice() + "," + t.getDate();}
				
		}
		return result;
	}
		
	public boolean equals(Object o) {
		if (items.equals(o)) {
			return true;
		}
		return false;
	}
	
	
    
}
	
	
