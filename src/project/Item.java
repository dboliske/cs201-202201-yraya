//Yosef Raya CS_201 4/28/2022 Final Project 
//Item class is a subclass of Age class since the user and some Items share the age attribute, also my store offer discount to certain user age groups so it is important for validation
//Item Class will be used to instantiate different item objects from the stock file 
//Each type of item (shelved, produce or age restricted) will display its information in accordance to its format in the stock file

package project;

public class Item extends Age {
	private String name;
	private double price;
	private String date;

	
	

	//default constructor includes all attributes
	public Item() {
		super();
		name = "pen";
		price = 1.79;
		date = "04/29/2022";
	}
	
		
	//Non-default constructor for shelved items
	public Item(String name, double price) {
	 this.name = name;
	 this.price = price;
	}
	
	//Non-default constructor for produce items with expiration date
	public Item(String name, double price, String date) {
		this.name = name;
		this.price = price;
		this.date = date;
	}
	//Non-default constructor for Age restricted items
	public Item(String name, double price, int age) {
		super(age);
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price > 0) {
		this.price = price;}
	}
	
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	
	@Override
	public String toString() {
		
		String result = null;
			
			 if (date == null) {
				 if (super.getAge() < 18) {result = name + "," + price;}
				 else if (super.getAge() == 18 || super.getAge() == 21) {result = name + "," + price + "," + super.getAge() ;}
				 }
			 
			else if (date.contains("/")) {result = name + "," + price + "," + date;}
				
		
		return result;
	}
	// a second to string method to display information about an Item (more user friendly)
	public String toString2() {
			String result = null;
			
			 if (date == null) {
				 if (super.getAge() < 18) {result = name + ", " + "price: " + "$" + price;}
				 else if (super.getAge() == 18 || super.getAge() == 21) {result = name + ", " + "Price: " + "$" + price + ", Age Required " + super.getAge();}
				 }
			 
			else if (date.contains("/")) {result = name + ", " + "Price: " + "$" + price + ", " + "Expiry date: " + date;}
				
			 return result;
	}

	//equals method to compare items using string important for searching for items
	//used almost in all options
	public boolean equals (String name) {
		if (name != this.name) {
			return false;
		}
		return true;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Item)) {
			return false;
		}
		
		Item p = (Item)obj;
		if (p.getName() != this.name) {
			return false;
		} else if (p.getPrice() != this.price) {
			return false;
		}
		return true;
	}
	
	//overridden method used to prompt the user to validate their age when they attempt to add an age restricted item to their cart
	//used in option 4. Go Shopping
	@Override
	public void validUserAge() {
		
			System.out.println("You must validate your age");
			System.out.println("Please Enter your age: ");
		
	}
	
	
}
