//Yosef Raya CS_201 4/28/2022 Final Project 
//Age class is the superClass, which the Item class will inherit the age attribute from
//It is important to validate user age for some items for legality issues, and certain user age groups qualify for discounts

package project;

public class Age {
	private int age;
	
	
	public Age() {
	age = 17;
	}

	public Age(int age) {
		this.age = age;
	}
	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
	public String toString() {
		return "" + age;
	}
	
	//special method to warn users of some age requirements 
	public void validUserAge() {
		System.out.println("Attention: Some items have age requirments, please have your ID ready to verfiy age");
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Age)) {
			return false;
		}
		
		Age a = (Age)obj;
		if (a.getAge() != this.age) {
			return false;
		} else if (a.getAge() != this.age) {
			return false;
		}
		return true;
	}
	
	//This method used to apply student discount on total after all items are added to cart
	//Used in option 4. Go Shopping
	public double discount(double total) {
		double studentDiscount = 0.20;
		double discount = 0;
		if (age < 18 && age > 13) {
			discount = total - (studentDiscount * total);
		}
		return discount;
	}
	
	
	
}
