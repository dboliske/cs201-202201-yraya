//Yosef Raya CS_201 4/4/2022 Lab 7 Exercise 1 
package labs.lab7;

public class Exercise1 {
//create a bubble sort method to call it in the main method to sort the array of interest
//bubble sort algorithm compares 2 adjacent elements and swaps them if the first > second
//continue this process until all is in the right place
//Not super efficient 
	
	public static int[] bubbleSort(int[] bubble) {
		boolean done = false; 
		
		do {											//1. Use a do while loop controlled flag to iterate at least once to check if array is organized
			done = true; 								//2. set done to true in case array is sorted to end the loop
			for (int i=0; i<bubble.length - 1; i++) {	//3. create a for inner loop to go through the array
				if (bubble[i+1] < bubble[i]) {			//4. an if statement to compare the adjacent elements 
					int temp = bubble[i+1];				//5. create a temporary variable to use as a reference point and store the new value of interest
					bubble[i+1] = bubble[i];			//6. swap elements
					bubble[i] = temp;					
					done = false;						//7. in the case of a swap done turns into false to allow the do-while loop to iterate again
				}
			}
		} while(!done);
		
		return bubble;
	}
	
	
	public static void main(String[] args) {
		int[] numbers = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
	numbers = bubbleSort(numbers);
		
		for (int n : numbers) {
			System.out.print(n + " ");
		}

	}

}

