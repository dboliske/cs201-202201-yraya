//Yosef Raya CS_201 4/4/2022 Lab 7 Exercise 4
package labs.lab7;

import java.util.Scanner;

public class Exercise4 {
	//creating a method that takes string array, start of array, end of array, and a string of interest

	public static int search(String[] array, int start, int end, String word) {
		if (end >= start) {										//an if statement to confirm that the array has elements to determine the midpoint 
			int middle = (start + end)/ 2; 						//middle is start + end /2
		
			if (array[middle].equalsIgnoreCase(word)) {			//if middle is the word we are looking for then return middle
				return middle; 
			} 
			
			if (array[middle].compareToIgnoreCase(word) > 0) {	//if the middle compared to the word is > 0 then we can look in the first half of the array
				return search(array, start, middle - 1, word);	//calling our method to search though the first half indicated by (middle - 1)
			} else {
				return search(array, middle + 1, end, word);	//else it will be in the second half 
			}
		}
		return -1;												//if we get to this part of the code then word is not found
	}
	//notice here no loops were used where the method goes through the array by giving it points of search in its argument
	
	
	
	
	public static void main(String[] args) {
		String[] data = {"c", "html", "java", "python", "ruby", "scala"};
		int n = data.length;							// n was assigned to the length of our array so we can include it in the method's argument
		Scanner input = new Scanner(System.in);
		System.out.print("Please insert the word you are looking for: ");
		String a = input.nextLine();
		
		int index = search(data, 0, n-1, a);			//declared index as an int and assigned the to the method above to be able to print it
		
		if (index == -1) {								//here we take the result of the method and apply these terms to it
			System.out.println("Not found");
		} else {
			System.out.println(a + " is found at index " + index + ".");
		}
	input.close();
	}

}