//Yosef Raya CS_201 4/4/2022 Lab 7 Exercise 2
package labs.lab7;

public class Exercise2 {

	public static String[] insertSort(String[] words) {
		for (int j=0; j<words.length - 1; j++) {				//a main for loop to check which element needs to be swapped in the correct spot
			int i = j;											//j counter is used to know where the element was moved from, i refers to where it'll be placed, initially at j
			while (i > 0 && words[i].compareTo(words[i-1])< 0) {// inner loop to loop over things and move it to the right spot, while i > 0 (front) keep moving,  
				String temp = words[i];							//compareTo method compares strings lexicographically, meaning here if element @ i is before element @ i-1 (since < 0), then swap
				words[i] = words[i-1];							//swap
				words[i-1] = temp;								//i-1 has a new element on it
				i--;											//increment by one since we moved the newly sorted element to i-1 
			}
		}
		return words; 
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		String[] words = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		words = insertSort(words);
		
		for (String w : words) {
		System.out.print(w + " ");
		}

	}

}
//Think of your data as two sections, one sorted and the other is not
//attempts to sort the next element in reference to what was sorted already 