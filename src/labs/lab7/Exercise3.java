//Yosef Raya CS_201 4/4/2022 Lab 7 Exercise 3
package labs.lab7;

public class Exercise3 {
	//it goes though the entire section finds the smallest and puts it at first and so on
	
	public static double[] selectSort(double[] numbers) {
		for (int i=0; i<numbers.length - 1; i++) {//creating a loop to go through the array 
			int min = i;
			
			for (int j = i+1; j<numbers.length; j++) {//find the minimum 
				if (numbers[j] < numbers[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				double temp = numbers[i];//swap here when minimum is found
				numbers[i] = numbers[min];
				numbers[min] = temp;
			}
		}
		
		return numbers; 
	}
	
	
	public static void main(String[] args) {
		double[] numbers = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		numbers = selectSort(numbers);
		
		for (double n : numbers) {
		System.out.print(n + " ");
		}

	}
}
