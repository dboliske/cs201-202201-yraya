package labs.lab5;

import java.io.File;
import java.util.Scanner;
public class CTAStopApp {
	
	//-------------------------------------------------------------------------------
	//read file method
	public static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[34];//construct our array to read the file
		
		try {//adding the try & catch method to catch errors since we are reading from a file
			File f = new File(filename);
			Scanner input = new Scanner(f);//scanner to read file from user input
			int count = 0;
			while (input.hasNextLine()) {//loop to go through our file
				try {
				String line = input.nextLine();
				String[] values = line.split(",");//using split method to access values in every array element
				CTAStation c = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
				stations[count] = c;
				count++;
				} catch (Exception e) {}
				
			}
			input.close();
		}  catch (Exception e) {
			System.out.println("Error occured while reading in file");
		}
		return stations;
	}
	
	//-------------------------------------------------------------------------------
	//creating the menu method
	public static CTAStation[] menu(Scanner input, CTAStation[] stations) {
		boolean done = false;//boolean to control our while flag loop
		do {//using do while loop to continue prompting the user
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": 
					displayStationNames(stations);//called the method created below with ssociated argument
					break;
					
				case "2": 
					displayByWheelChair(input, stations);
					break;
					
				case "3": 
					displayNearest(stations, input);
					break;
					
				case "4": //exits
					done = true;
					break;
					
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
			
		} while (!done);
		
		input.close();
		
		return stations;
	}
	
			
	//-------------------------------------------------------------------------------
	
	//display names method
	public static void displayStationNames(CTAStation[] stations) {							//this returns void since no values being returned
		for (int i=1; i<stations.length; i++) {
			System.out.println(stations[i]);
		}	
	}
	
	//-------------------------------------------------------------------------------
	
	//display by wheelchair method
	public static void displayByWheelChair(Scanner input, CTAStation[] stations) {
		 while (true) {
		  System.out.println("Would you like the station to accessable for wheelchairs?");
		  System.out.print("please choose Y or N");
		  String answer = input.nextLine();

		  if (answer.equalsIgnoreCase("y")) {												//this if statement is for if answer was y all the stuff inside this will happen
			  	boolean found = false;
		   for (CTAStation station : stations) {											//created an array object station to store entries that have wheelchair  
			if (station.hasWheelchair()) {													//if statement inside for loop to pick and choose also called the hasWheelchair method from out subclass
		     found = true;																	//our value hasWheelchair from the stations array has to = true to be stored in our station array 
		     System.out.println(station);
		    }
		   }
		   if (!found) {																	//if no station is found which means 
		    System.out.println("Sorry there is no stations with wheelchair accessibility");
		   }
		   
		   return;//this return is to go back to the main menu after this is done 

		  } else if (answer.equalsIgnoreCase("n")) {//else if statement created for the n answer follows the if statement associated with answering y
		   boolean found = false;
		   for (CTAStation station : stations) {
		    if (!station.hasWheelchair()) {
		     found = true;
		     System.out.println(station);
		    }
		   }

		   if (!found) {
		    System.out.println("Sorry there is no stations without wheelchair accessibility");
		   }
		   return;//to go back to main menu
		   }
		 }
		}
	
	//-------------------------------------------------------------------------------
	//display nearest 
	public static void displayNearest(CTAStation[] stations, Scanner input) {
		
		input = new Scanner(System.in);
		
		System.out.println("Please Enter your Geolocation: ");
		System.out.println("Please Enter your Latitude: ");
		
		double lat=0;
		try {
		lat = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
				System.out.println("Invalid input, please enter a double");
			}
		
		System.out.println("Please Enter your Longitude : ");
		double lng = 0; 
		try {
			 lng = Double.parseDouble(input.nextLine());
			} catch (Exception e) {
					System.out.println("Invalid input, please enter a double");
				}
	
				int m = 0;
			
				for (int i=1; i<stations.length; i++) {
					if(stations[i].calcDistance(stations[i].getLat(), stations[i].getLng()) < stations[m].calcDistance(lat, lng)) {m = i;}
				}
				
		System.out.println(stations[m]);
		
		input.close();
			return;//I get an error after I complete this calculation even though it displays the nearest station correctly
			//I am not sure why, it seems like it is not returning back to the main menu
		}
		
	
	//-------------------------------------------------------------------------------
	
	public static void main(String[] args) {//main method
		Scanner input = new Scanner(System.in);//Scanner to get file name
		System.out.print("Insert file name: ");
		String filename = input.nextLine();
		CTAStation[] stations = readFile(filename);
		stations = menu(input, stations);//passing the data to the menu
		input.close();
		System.out.println("GoodBye");
		
	}

	
}
//file name: src/labs/lab5/CTAStops.csv 
