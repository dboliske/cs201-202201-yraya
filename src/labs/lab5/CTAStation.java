package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStation extends GeoLocation {
	private String name;
	private String location; 
	private boolean wheelchair; 
	private boolean open;
	
	public CTAStation() { //default constructor 
		super();//used to initialize attributes from the super class
		name = "Lawrance Stop";
		location = "Elevated";
		wheelchair = false;
		open = false;
		
	}
	
	//in the non-default constructor you include the values that are coming in from super class
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);//setting the instance variables that can be set in the super class
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;	
	}
	
	//set methods for CTAStation subclass
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location; 
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	//get methods for CTAStation subclass
	public String getName() {
		return name;
	}
	public String getLocation() {
		return location;
	}
	public boolean hasWheelchair() {//using has here so the get method can make grammatical sense   
		return wheelchair;
	}
	public boolean isOpen() {//we use is here just so the get method can make grammatical sense
		return open;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {return false;}
		else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation c = (CTAStation)obj;
		if (this.name != c.getName()) {return false;}
		else if (this.location != c.getLocation()) {return false;}
		else if (this.wheelchair != c.hasWheelchair()) {return false;}
		else if (this.open != c.isOpen()) {return false;}
		return true;
	}
	@Override
	public String toString() {
		return "Station Name: " + name + " | Geolocation: " + super.toString() + " | Location: " + location + " | Wheel Chair Accessability: " + wheelchair +  " | Station is open: " + open;
	}
	
}
