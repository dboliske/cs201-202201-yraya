//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 2 part 2
package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber p1 = new PhoneNumber();//used default constructor
		System.out.println(p1.toString());//calling the toString method
		
		PhoneNumber p2 = new PhoneNumber("972", "312", "8765098");//used non-default constructor
		System.out.println(p2.toString());
		
		//p2.setCountryCode("667");//test our set method
		//p2.setAreaCode("221");
		//p2.setNumber("6547393");
		//System.out.println(p2);
		
		//p2.setAreaCode("98763"); when areaCode is set to a 4 string length > 3 it voids the entry
		//System.out.println(p2.toString());
		
		//System.out.println(p2.validaCode("65")); testing our validaCode method
		//System.out.println(p2.validNumber("65745446"));  testing our validNumber method
		
		//System.out.println(p1.equals(p2));
	}

}
