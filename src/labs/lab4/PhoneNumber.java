//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 2 
package labs.lab4;

public class PhoneNumber {
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "001";
		areaCode = "773";
		number = "8761243";
	}
	public PhoneNumber(String cCode, String aCode, String num) {
		countryCode = "001";
		setCountryCode(cCode);
		areaCode = "773";
		setAreaCode(aCode);
		number = "8761243";
		setNumber(num);
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public void setAreaCode(String areaCode) {
		if (areaCode.length() == 3)
		this.areaCode = areaCode;}
	
	public void setNumber(String number) {
		if (number.length() == 7) 
		this.number = number;
	}
	
	public String toString() {
		return countryCode + "-" + areaCode + "-" + number;
		
	}
	
	public boolean validaCode(String areaCode) {
		if (areaCode.length() == 3) 
		{return true;}
	return false; 
	}
	public boolean validNumber(String number) {
		if (number.length() == 7) 
		{return true;}
	return false; 
	}
	
	public boolean equals(PhoneNumber p) {
		if (this.countryCode != p.getCountryCode()) 
		{return false;}
			else if (this.areaCode != p.getAreaCode())
			{return false;}
				else if (this.number != p.getNumber()) 
				{return false;}
	return true;
	}
	
	}


