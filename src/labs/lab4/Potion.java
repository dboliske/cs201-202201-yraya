//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 3 
package labs.lab4;

public class Potion {
	private String name;
	private double strength;
	
	public Potion() {//non-default constructor
		name = "Omega";
		strength = 10.00;
	}
	
	public Potion(String n, double s) {//default constructor
		name = "Omega";
		setName(n);
		strength = 10;
		setStrength(s);
	}
	public String getName() {
		return name;
	}
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) {
	    String firstLetter = name.substring(0, 1);
	    String remainingLetters = name.substring(1, name.length());
	    firstLetter = firstLetter.toUpperCase();
	    name = firstLetter + remainingLetters;
		this.name = name;
	}
	
	public void setStrength(double strength) {
		if(strength <= 10 && strength >= 0)
		this.strength = strength;
	}
	
	public String toString() {
		return "This " + name + " Potion " + "has a strength of " + strength;
	}
	
	public boolean validStrength(double strength) {
		if(strength < 0 && strength > 10) 
		{return false;}
		return true;
	}
	
	public boolean equals(Potion p) {
		if (this.strength != p.getStrength())
		{return false;}
		return true;
	}
}
