//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 1 part 2
package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation g1 = new GeoLocation();//default constructor
		System.out.println("(" + g1.getLat() + "," + " " + g1.getLng()+ ")");//or I can use the toString special method

		GeoLocation g2 = new GeoLocation(63, 101);//using non-default constructor
		System.out.println("(" + g2.getLat() + "," + " " + g2.getLng()+ ")");//displaying values from non-default constructor
		
		
		//g2.setLat(75);//testing the setter method
		//g2.setLng(190);//the program voids this and keeps previous entry since it is out of the specified range
		//System.out.println(g2);//I do not have to include the toString method here since it is defined in GeoLocation
		
		//GeoLocation g3 = new GeoLocation(95, 190);
		//System.out.println(g3.validLat(95));//testing our validLat method
		//System.out.println(g3.validLng(190));//testing our validLng method
		
		//System.out.println(g2.equals(g3)); //testing the equals method
		System.out.println(g2.calcDistance(g1));
		System.out.println(g2.calcDistance(175, 50));
		
		
	}
}


