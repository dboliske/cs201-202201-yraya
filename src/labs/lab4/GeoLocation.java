//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 1 
package labs.lab4;

public class GeoLocation {
		private double lat;//declaring attribute,  data type, variable and name (lower case)
		private double lng;
		
		public GeoLocation () {//default constructor 
			lat = 90.0000000;
			lng = 180.0000000;	
		}
		public GeoLocation(double la, double lon){//non-default constructor with two instance variables
			lat = 90.0000000;
			setLat(la);
			lng = 180.0000000;
			setLng(lon);
		}	
		
		public double getLat() {//accessor method or getters
			return lat;
		}
		public double getLng() {//accessor method or getters
			return lng;
		}
		
		public void setLat(double lat) {//mutator method or setters
			if  (lat>=-90 && lat<=90)
				this.lat= lat;
		}
		public void setLng(double lng) {//mutator method or setters
			if  (lng>=-180 && lng<=180)
				this.lng= lng;
		}	
		
		public String toString() {//toString special method
			return "(" + lat + "," + " " + lng + ")";
		}
		
		public boolean validLat(double lat) {
			if (lat >= -90 && lat <= 90) 
			{return true;}
			
			return false;}
			
			public boolean validLng(double lng) {
				if (lng >= -180 && lng <= 180) 
				{return true;}
				
				return false;
		}
		
		public boolean equals(GeoLocation g) {//equals method
			if (this.lat != g.getLat())
				{return false;}
				else if (this.lng != g.getLng()) 
				{return false;}
			
			return true;	
	}
		
		public double calcDistance(GeoLocation g) {
			 return Math.sqrt((this.getLat() - g.getLat()) * (this.getLat() - g.getLat())
					+ (this.getLng() - g.getLng()) * (this.getLng() - g.getLng()));
			 
		}
		
		public double calcDistance(double lat, double lng) {
			return Math.sqrt((this.getLat() - lat) * (this.getLat() - lat)
					+ (this.getLng() - lng) * (this.getLng() - lng));
		}
}



