//Yosef Raya CS_201 2/20/2022 Lab 4 Exercise 3 part 2
package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion();//used default constructor
		System.out.println(p1.toString());//calling the toString method
		
		Potion p2 = new Potion("Alpha", 8);//used non-default constructor
		System.out.println(p2.toString());
		
		
		//p2.setName("beta");//test our set method that converts first letter to capital
		//p2.setStrength(9);
		//p2.setStrength("11");//to test if out set method voids this entry
		//System.out.println(p2);
		
		
		//System.out.println(p2.ValidStrength("65")); testing our validaCode method, shall return false

		
		//System.out.println(p1.equals(p2)); //test the equals method, shall return false
	}

}