//Yosef Raya CS_201 3/21/2022 Lab 6  
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class CustomerQueueApp {
	
	static Scanner input = new Scanner(System.in);						//used a static scanner to be used in all methods 
//===================================================================================================	
	//The menu method
	public void menu(ArrayList<String> customerQueue) {								//1. Declared the menu method that does not return anything since nothing is being saved to a file
		 input = new Scanner(System.in);
		int choice= 0;
		
		while(choice !=3) {															//2. Used flag controlled while loop, exits at option 3  
			System.out.println("Please choose one of these following options: ");
			String[] options = {"Add customer to queue", "Help customer", "Exit"};	//3. Created a String array to host menu options then use a loop for prompting
			for (int i=0; i<options.length; i++) {									//4. Used a for loop inside the while loop to print out menu options. 
				System.out.println((i+1) + ". " + options[i]);}
			
			choice = input.nextInt();												//5. Used if else statements to navigate the menu options 
			if (choice == 1) {addCustomer(customerQueue);}							//6. called the addCustomer
				else if (choice == 2 && customerQueue.size() != 0) {helpCustomer(customerQueue);}
					else if (choice != 3) {
						System.out.println("Please insert a valid choice:" );}
		}
		
	}
	
//====================================================================================================		
	//The add customer method that returns the customer position
	public int addCustomer(ArrayList<String> CustomerQueue) {			//1. Declared the method that takes an array list as an argument and returns an integer 
		 input = new Scanner(System.in);								//2. Scanner already declared just call it using the variable input
		System.out.println("Please input name: ");						
		String name = input.nextLine();									
		CustomerQueue.add(name);										//3. Called the add() method from the arrayList imported class to our declared arrayList
		int size = CustomerQueue.size();								//4. To know the added customer queue we just check the count of elements in the array list using size() associated with arrayList class
		System.out.println("Hello " + name + "!");						
		System.out.println("Your Queue is number: " + size);
		return CustomerQueue.size();
		
	}
//====================================================================================================	
	//The Help option method removed customer in queue 1 and outputs the name to the console
	public void helpCustomer(ArrayList<String> CustomerQueue) {		//1. The method has to take the arrayList as an argument
		String name = CustomerQueue.remove(0);						//2. To remove the first customer in queue we just remove the first element in the list @ 0
		System.out.println("You are in " + name);					//3 Prints it to console
		System.out.println("Please come to the counter to place your order, Thanks!");
		System.out.println(" ");
	} 
	
//====================================================================================================		
	//The main method
	public static void main(String[] args){							// main method that runs the program
		ArrayList<String> Customer = new ArrayList<String>();		//Declared the array list that the menu method will take
		CustomerQueueApp store = new CustomerQueueApp();			//instantiated an object of our current created class of CustomerQueueApp to pass it to the menu method 
		store.menu(Customer);										//store is an instance of the CustomerQueueApp object is applied to the menu method so it can be able to call the methods inside that object
																	//menu is the method declared above that takes an arraylist (Customer) as an argument
		
	}

}
