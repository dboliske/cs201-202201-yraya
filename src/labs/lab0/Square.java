//Yosef Raya CS_201 1/28/2022 Square
package labs.lab0;
//Use the letter s as a building block for my square 
//the size of the square will be 4x4 bytes
//the first line of code should output 4 s characters 
//the second line of code should output 2 s characters and 2 spaces
//the third line of code should output 2 s characters and 2 spaces
//the fourth line of code should output 4 s characters 
public class Square {
 
public static void main(String[] args) {
System.out.println("s" + "s"+ "s" + "s");
System.out.println("s" + " "+ " " + "s");
System.out.println("s" + " "+ " " + "s");
System.out.println("s" + "s"+ "s" + "s");
}
 
}
