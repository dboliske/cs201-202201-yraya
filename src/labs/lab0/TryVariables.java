//Yosef Raya CS_201 1/28/2022 Try Variables
//examples of primitive data types 
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;//32 bits
		long large = 42561230L;//64 bits
		short small = 25;//16 bits
		byte tiny = 19;//8 bits

		float f = 0.0925F;//F must be added at the end of each float, 32 bits
	    double decimal = 0.725;//doubles must have a left digit, 64 bits   
		double largeDouble = +6.022E23; // This is in scientific notation, 64 bits

		char character = 'A';//16 bits
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small);
		System.out.println("tiny is " + tiny);
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal);
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character);
		System.out.println("t is " + t);

	}

}