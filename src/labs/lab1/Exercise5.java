//Yosef Raya CS_201 1/30/2022 Exercise 5
package labs.lab1;

import java.util.Scanner;

public class Exercise5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Lenght of box");
		double L = input.nextDouble();
			
			
		System.out.println("Width of box");
		double W = input.nextDouble();
			
					
		System.out.println("Depth of box");
		double D = input.nextDouble();
		
		L = L/12;
		W = W/12;
		D = D/12;
		
		double A; 
		
		A = (2*(L*W)) + (2*(L*D)) + (2*(W*D));
	
		System.out.println("Area of the box = " + A + " sf");

	}

}
