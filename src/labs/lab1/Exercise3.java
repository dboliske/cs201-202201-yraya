//Yosef Raya CS_201 1/30/2022 Exercise 3
package labs.lab1;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//Scanner created to read input from user
		System.out.println("First Name:");
		
		String name = input.nextLine();// string declared for next line input
		
		char FI = name.charAt(0);
		
		System.out.println("First Initial: " + FI);
		
	}


}
