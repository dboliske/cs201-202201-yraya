//Yosef Raya CS_201 1/30/2022 Exercise 4
package labs.lab1;

import java.util.Scanner;

public class Exercise4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//Scanner created to read input from user
		System.out.println("temperature in Fahrenheit:");
		
		int t = input.nextInt();// string declared for next line input
		//1 C = 5/9 * t - 32
		int c = (t - 32) * 5/9; 
		System.out.println("temperature in Celsius = " + c);

	}

}
