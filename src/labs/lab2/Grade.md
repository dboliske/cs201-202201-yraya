# Lab 2

## Total

17/20

## Break Down

* Exercise 1    6/6
* Exercise 2    4/6
* Exercise 3    5/6
* Documentation 2/2

## Comments
- Ex2 doesn't handle floating points (decimals)
- Ex2 calculates average wrong
- Ex3 doesn't handle floating points (decimals)