//Yosef Raya CS_201 2/1/2022 Exercise 3
package labs.lab2;

import java.util.Scanner;

public class Excercise3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		boolean done = false;
		
		while (!done) {
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.println("Choice");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1":
				System.out.println("Hello!");
				break;
			case "2":
				System.out.println("Enter value 1: ");
				int n = input.nextInt();
				System.out.println("Enter value 2: ");
				int s = input.nextInt();
				System.out.println("The Answer is =" + (n+s));
				break;
			case "3":
				System.out.println("Enter value 1: ");
				int a = input.nextInt();
				System.out.println("Enter value 2: ");
				int h = input.nextInt();
				System.out.println("The Answer is " + (a*h));
				break;
			case "4": 
				done = true;
				break;}
				
		}
		System.out.println("Program Terminated");
		}

	}


