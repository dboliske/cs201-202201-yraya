//Yosef Raya CS_201 2/1/2022 Exercise 1 
package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
	System.out.print("Square Dimension: ");
	int d = input.nextInt();
	
	for (int i=0; i<d; i++) 
	{for (int j=0; j<d; j++) {System.out.print("* ");}
	
	System.out.println();}
	
	input.close();
	}

}
