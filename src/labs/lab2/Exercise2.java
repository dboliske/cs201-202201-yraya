//Yosef Raya CS_201 2/1/2022 Exercise 2 
package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		
				float sum = 0;
				int i = 0;
				
				float grade = 0;
	        while(grade !=-1) {
	            System.out.println("Enter grades or [-1] when done: ");
	            grade = input.nextInt();
	            sum += grade; 
	            i++;
	            if(grade == -1){break;}
	            }
	        float average = sum/(i-1);
	        System.out.println("Student Grades Average =  " + average);
		
		
		
		
		}
	}
		
	
//data sets test
//input					  expected result				Actual result 
//70, 60						64.5						64.4
//90,80,75,60					76.25						76
//72,81,89,69,92				80.6						80.4