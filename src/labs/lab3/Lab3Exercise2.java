//Yosef Raya CS_201 2/8/2022 Lab 3 Exercise 2 
package labs.lab3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Lab3Exercise2 {

	public static void main(String[] args) {
		int[] values = new int[1000];//create an array to store values in
		Scanner input = new Scanner(System.in);
		int size=0;//to define the actual size of the array based on user input
		
		while(true) {//use while loop to break when user enters done
			System.out.println("Enter a Number, or write 'done' when completed: ");
			String UserInput = input.nextLine();//to read user input
			if (UserInput.equals("done")) {//breaking point
				break;}
			else {
				values[size]=Integer.parseInt(UserInput);//converting input string to int and add it to values array
			}
			size++;
		}
		
		System.out.println("Enter New File Name: ");
		String fileName = input.nextLine();
		try {FileWriter writer = new FileWriter(fileName,true);
		for(int j=0; j<size; j++) {writer.write(values[j] + "\n");}
		writer.close();
		}
		catch (IOException e) {e.printStackTrace();
		}
		System.out.println("Numbers Saved Successfully");
		
		
		input.close();
		
	}
	
}
