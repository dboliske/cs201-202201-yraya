//Yosef Raya CS_201 2/8/2022 Lab 3 Exercise 3 
package labs.lab3;

public class Lab3Exercise3 {

public static void main(String[] args) {
 int arr[] = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111,
			 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 
			 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};//created an int type array using given data
		 
	 int min = arr[0];//declaring the minimum and to start we equate to the first element of the array
	 for (int i=0; i<arr.length; i++) //use a for loop to run through the array
	 {if (i<min)//using an if statement to compare the following element to the current minimum
	 {min = arr[i];}//assigning the lowest element to the min variable
	 }
	  System.out.println("Minimum Number in Array: " + min);//out putting the result
	        }
	}

