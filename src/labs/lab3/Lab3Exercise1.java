//Yosef Raya CS_201 2/8/2022 Lab 3 Exercise 1
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Lab3Exercise1 {

	public static void main(String[] args) throws IOException {
		File grades = new File("src/project/stock.csv");//Link the wanted file
		Scanner input = new Scanner(grades);//create a scanner to read from file as input
		int[] data = new int[14]; //creating an array to store the values in 
		int count = 0;
		while (input.hasNextLine()) {//creating a while loop with .hasNextLine to go through data in file
			String line = input.nextLine();//create a string to read one line at a time 
			String[] values = line.split(",");//split method used to break a string into parts using a character 
			data[count] = Integer.parseInt(values[1]);//reads the value at counter 1 and stores in the array
			count++;
		}
		input.close();
		
		double sum = 0;
		for (int i=0; i<data.length; i++) {//create a loop to sum values
			sum += data[i];//the new value of sum updates while loop goes through array
			
		}
		
		System.out.println(sum/data.length);//output average by dividing sum over total number of grades
	}

}
