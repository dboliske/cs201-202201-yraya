//Yoser Raya Midterm Exam CS-201-02 (Online Section) 2/25/2022 
//Question Three

package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter your triangle dimension: ");
		
		
		try {int d = Integer.parseInt(input.nextLine());
			 int i; 
		
			 for ( i = 0; i<d; i++) {
			
				for (int j=0; j<(d-i); j++) {
					 System.out.print("* ");}
			
			System.out.println();
			
				for (int y=0; y<(i+1); y++) 
				{System.out.print("  ");}
			 }
		
		    } catch (NumberFormatException e) {
			System.out.println("Please Enter an Integer!");
		}
		
		input.close();
		

	}

}
