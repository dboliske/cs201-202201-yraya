//Yoser Raya Midterm Exam CS-201-02 (Online Section) 2/25/2022 
//Question One

package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
		System.out.print("Please Enter a Number: ");
		
			try {int n = Integer.parseInt(input.nextLine());
				 int c = n + 65;
				 System.out.print((char)c);
				 done = true;
				 break;}
		
			catch (NumberFormatException e)
			{
				System.out.println("Invalid input, Please make sure you enter an Integer: ");
			}
		
	  }while(!done);
		
input.close();
		
		
//I added a few extra features to the program requested:
//I integrated the try/catch syntax to verify the user input is an Integer 
//I also created a do while loop to be activated when an exception occurs 
//to continue on prompting the user to enter an Integer 
	}

}
