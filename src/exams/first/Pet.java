//Yoser Raya Midterm Exam CS-201-02 (Online Section) 2/25/2022 
//Question Five

package exams.first;

public class Pet {
private String name;
private int age; 

public Pet() {
	name = "Lucy";
	age = 4;
}

public Pet(String n, int a) {
	name = "Lucy";
	setName(n);
	age = 4;
	setAge(a);
}


public void setName(String name) {
	String first = name.substring(0, 1);
	String rest = name.substring(1, name.length());
	first = first.toUpperCase();
	name = first + rest;
	this.name = name;	
}
public void setAge(int age) {
	if (age > 0) 
		this.age = age;
}


public String getName() {
	return name;
}
public int getAge() {
	return age;
}


public boolean equalsObject(Pet p) {
	if (this.name.equals(p.getName()) && this.age == p.getAge())
		{return true;}
	
	return false;
}


public String toString() {
	
	return "Pet name: " + name + "\n" + "Age: " + age;
}

}
