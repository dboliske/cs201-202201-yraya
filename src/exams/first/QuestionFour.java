//Yoser Raya Midterm Exam CS-201-02 (Online Section) 2/25/2022 
//Question Four

package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[] arr = new String [5];
		
		System.out.println("Please enter 5 words: ");
		
		for (int i=0; i<arr.length; i++) {//loop to store input into array
			arr [i] = input.nextLine();
		}
		
		input.close();
		
		//we have to create a nested loop to perform two tasks at once 
		
		for (int j=0; j<arr.length; j++) {//to read through the array
			
				for (int i=j+1; i<arr.length; i++) //loop to compare elements and the counter i=j+1 to compare to the next element
					if (arr[j].equals(arr[i])) 
						{System.out.println(arr[i]);}
				}
		}

	}


