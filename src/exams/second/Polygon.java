//Yosef Raya
package exams.second;

public abstract class Polygon {
	protected String name;
	
	public Polygon() {
		name = "Polygon";
	}
	
	public Polygon(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	public abstract double area();

	
	public abstract double perimeter();

		
	
}
