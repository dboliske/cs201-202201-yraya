//Yosef Raya
package exams.second;

public class ComputerLab extends ClassRoom{
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}
	public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
		super(building, roomNumber, seats);
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Computers Availability: " + computers;
	}
	
}
