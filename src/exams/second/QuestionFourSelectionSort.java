//Yosef Raya
package exams.second;

public class QuestionFourSelectionSort {

	public static void selectSort(String[] words) {
		
		for (int i=0; i<words.length; i++) {//creating a loop to go through the array 
			int firstIndex = i; //start from first index of the array
			String firstString = words[i];// assign the first word to the first index
			
			for (int j = i+1; j<words.length; j++) {//an inner loop to compare the first word with the one after 
				if (words[j].compareTo(firstString) < 0) {//compareTo method that compares strings, if result is less than 0, means that our string comes after the string we compares to
					firstIndex = j;//update index to the new one to start looping from there
					firstString = words[j];//update our word with the new one found (swap)
				}
			}
			
		words[firstIndex] = words[i];//new start, loop to compare to the next work
		words[i] = firstString;
			
		}
		
		
	}
	
	
	public static void main(String[] args) {
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		selectSort(words);
		
		System.out.println("Sorted Array: ");
		for (String n : words) {
		System.out.print(n + " ");
		}

	}
}
