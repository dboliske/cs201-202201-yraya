//Yosef Raya
package exams.second;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class QuestionFiveJumpSearch {
	
	
	public static int search(ArrayList<Double> numbers, int step, int newEndOfStep, double target) {
	
	    //our base case where if the updated skip index is less than the length of the array, and our target is still larger than all the elements we skipped to, return -1
	    if (newEndOfStep < numbers.size() && target > numbers.get(newEndOfStep)) {
	    	
	    	newEndOfStep = (int) (newEndOfStep + step);// needs to be casted to use in loop and to represent an index since we are dealing with a square root
	       
	        return search(numbers, step, newEndOfStep, target);//keep skipping until finding an element that is larger than the target element
	        
	    } else { //if larger element found, we have to go back to the start of the last skip and search from there up
	    	for (int i = newEndOfStep - step + 1; i <= newEndOfStep && i < numbers.size(); i++) {//for loop to perform linear search starting from the beginning of the last skip
	            
	    		if (target == numbers.get(i)) {//if any element equals our target element then return the index i
	                return i;
	            }
	        }
	    }
	
	    return -1;
	}
	
	public static int searchForNumber(ArrayList<Double> numbers, double target) {//had to do the initial calculations in a separate method 
		
	int skip = (int) Math.sqrt(numbers.size());//square root of array length will be the size of our skip since it is the most optimal efficiency

	   
	int newEndOfskip = skip-1;//The first skip will land at index that equals the size of the skip -1, which marks our new point of search			
								//This will be updated for every skip when the target is not found
	return search(numbers, skip, newEndOfskip, target);
	}
	
	
	
public static void main(String[] args) {
		
		ArrayList<Double> numbers = new ArrayList<> (Arrays.asList (0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142));
			
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter the number you would like to search: ");
		
		double number = Double.parseDouble(input.nextLine());
		
		int result = searchForNumber(numbers, number);
		
		System.out.println("The number is at index " + result);
		
		input.close();
	}

}
