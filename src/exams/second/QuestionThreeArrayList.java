package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThreeArrayList {
	
	public static double[] selectSort(double[] numbers) {	//used selection sort to sort array after user adds numbers
		for (int i=0; i<numbers.length - 1; i++) {			//creating a loop to go through the array 
			int min = i;
			
			for (int j = i+1; j<numbers.length; j++) {		//find the minimum 
				if (numbers[j] < numbers[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				double temp = numbers[i]					;//swap here when minimum is found
				numbers[i] = numbers[min];
				numbers[min] = temp;
			}
		}
		
		return numbers; 
	}
	
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		ArrayList<String> numbers = new ArrayList<>();
		String entry;
		double inputTry;
		
		do {
		System.out.println("Enter a number you would like to add to the list: " + "(Enter done when finished)");
		
		 entry = input.nextLine();
		 
		 if (entry.equals("done")) {//added a done breaker before converting to double in case user wants to quick program
			 break;
		 }
		 
		 try {inputTry = Double.parseDouble(entry);//added try catch to validate entry
		 ;
		 } catch (NumberFormatException ex) {//through exception if entry is invalid and quit program
			 System.out.println("You have been kicked out of the program");
			 System.out.print("INVALID ENTRY, Restart program and PLEASE ENTER NUMBERS ONLY!" + "\n");
			 return;
			 
			 }
		 
		 numbers.add(entry);//if entry is valid add to arayList
		 
		
		
		} while (entry != "done");
		
		numbers.remove("done");//removes done from arraylist since we only need numbers
			
			double[] arr = new double[numbers.size()];// create an array to sort numbers using selection sort
			
			for(int i=0; i<numbers.size(); i++) {//use a loop to enter numbers into array
				arr[i] = Double.parseDouble(numbers.get(i));
			}
			
			selectSort(arr);
			System.out.println("The minimum is: " + arr[0]);
			System.out.println("The maximum is: " + arr[arr.length-1]);
			
		input.close();
	}
}
		
		
		
	



