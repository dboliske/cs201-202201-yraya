package exams.second;

public class Rectangle extends Polygon {
	private double width;
	private double height;
	
	public Rectangle() {
		super();
		width = 1;
		height = 2;
	}
	
	public Rectangle(String name, double width, double height) {
		super(name);
		this.width = 1;
		setWidth(width);
		this.height = 1;
		setHeight(height);
	}
	
	public double getWidth() {
		return width;
	}


	public void setWidth(double width) {
		if (width > 0)
		this.width = width;
		else this.width = 1;
	}


	public double getHeight() {
		return height;
	}


	public void setHeight(double height) {
		if (height > 0)
		this.height = height;
			else this.height = 1;
	}


	@Override
	public double area() {//The area of a rectangle is height * width
		double area = width * height;
		return area;
	}

	@Override
	public double perimeter() {//the perimeter is 2.0 * (height + width)
		double perimeter = 2.0 * (height + width);
		return perimeter;
	}
	
	@Override
	public String toString() {
		String result = super.toString();
		
		return result + " with a width and a height of: " + width + ", " + height;
	}
	
	public String toStringForCalculation(String calculation) {
		
		String result = super.getName() + " Width and Height: " + width + ", " + height;
		
			result += "\n" + calculation + ": "; 
		
		return result;
	}
}
