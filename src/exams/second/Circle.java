//Yosef Raya
package exams.second;

public class Circle extends Polygon {
	private double radius;
	
	public Circle() {
		super();
		radius = 1.0;
	}
	
	public Circle(String name, double radius) {
		super(name);
		this.radius = 1;
		setRadius(radius);
	}
	public double getRadius() {
		return radius;
	}

	public void setRadius(double width) {
		if (width > 0)
		this.radius = width;
			else this.radius = 1;
	}
	
	@Override
	public String toString() {
		String result = super.toString();
		
		return result + " with a radius of: " + radius;
	}
	
	
	public String toStringForCalculation(String calculation) {
		
		String result = super.getName() + " Radius: " + radius;
		
			result += "\n" + calculation + ": "; 
		
		return result;
	}
	
	@Override
	public double area() {//Math.PI * radius * radius
		
		double area = Math.PI * radius * radius;
		
		return area;
	}

	@Override
	public double perimeter() {//2.0 * Math.PI * radius
		
		double perimeter = 2.0 * Math.PI * radius;
		return perimeter;
	}

}
