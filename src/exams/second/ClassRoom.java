//Yosef Raya
package exams.second;

public class ClassRoom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public ClassRoom() {
		building ="Main building";
		roomNumber = "101";
		seats = 5;
	}
	
	public ClassRoom(String building, String roomNumber, int seats) {
		this.building = building;
		this.roomNumber = roomNumber;
		this.seats = 5;
		setSeats(seats);
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats > 0) 
		this.seats = seats;
	}
	
	public String toString() {
		return "Building: " + building + ", Room Number: " + roomNumber + ", Number of seats: " + seats;
	}
	
}
